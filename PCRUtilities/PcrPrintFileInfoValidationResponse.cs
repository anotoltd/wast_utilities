﻿using System.Collections.Generic;

namespace PCRUtilities
{
    internal class PcrPrintFileInfoValidationResponse
    {
        public PcrPrintFileInfoValidationResponse()
        {
            ValidationErrors = new List<string>();
        }

        public List<string> ValidationErrors { get; private set; }
        public bool IsValid { get { return ValidationErrors.Count == 0; } }
    }
}