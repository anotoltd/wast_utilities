//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PCRUtilities.ALE
{
    using System;
    using System.Collections.Generic;
    
    public partial class form
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public form()
        {
            this.answers = new HashSet<answer>();
            this.form_submissions = new HashSet<form_submissions>();
        }
    
        public int billId { get; set; }
        public Nullable<long> penId { get; set; }
        public int appId { get; set; }
        public Nullable<int> version { get; set; }
        public bool correction { get; set; }
        public Nullable<int> originalBillId { get; set; }
        public string @ref { get; set; }
        public string ref2 { get; set; }
        public Nullable<System.Guid> formGuid { get; set; }
        public string magicBoxPageAddress { get; set; }
        public bool encrypted { get; set; }
        public bool logicalBook { get; set; }
        public System.DateTime received_timestamp_utc { get; set; }
        public string penOwnerName { get; set; }
        public string penOwnerEmail { get; set; }
        public string penOwnerMobile { get; set; }
        public string penSoftwareVersion { get; set; }
        public string penProtocolVersion { get; set; }
        public string penTransport { get; set; }
        public string penModem { get; set; }
        public string penModel { get; set; }
        public Nullable<System.DateTime> start_timestamp_utc { get; set; }
        public Nullable<System.DateTime> end_timestamp_utc { get; set; }
        public Nullable<int> size { get; set; }
        public Nullable<int> formFields { get; set; }
        public Nullable<int> filledFields { get; set; }
        public Nullable<int> extraStrokes { get; set; }
        public bool sigValidated { get; set; }
        public bool empty { get; set; }
        public bool @void { get; set; }
        public bool invalid { get; set; }
        public string invalidReason { get; set; }
        public string soapStatus { get; set; }
        public System.DateTime recordCreatedDate_utc { get; set; }
        public bool deleted { get; set; }
        public string pgcFileName { get; set; }
        public Nullable<long> uniqueStartAddressId { get; set; }
        public string FormUnique { get; set; }
        public Nullable<int> cms_userId { get; set; }
        public Nullable<double> latitude { get; set; }
        public Nullable<double> longitude { get; set; }
        public Nullable<System.Guid> routerSessionGuid { get; set; }
        public Nullable<System.DateTime> process_start_utc { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<answer> answers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<form_submissions> form_submissions { get; set; }
        public virtual application application { get; set; }
        public virtual cms_users cms_users { get; set; }
    }
}
