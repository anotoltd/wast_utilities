﻿using log4net;
using Newtonsoft.Json;
using PCRUtilities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PcrPrintFileTrawler
{
    internal class PdfFileParser
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PdfFileParser));

        internal static void ParsePDFFilesAsync(string searchRoot = "E:", string searchPattern = "WAST PCR Prepop Live V2", 
            string outputFile = @"C:\Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\PcrIndex.json", int displayUpdateInterval= 100, 
            int newFileInterval = 1000, int mergeUpdateInterval = 10000)
        {
            //await PdfFileResolver.GetInformationFromFileAsync(new FileInfo(@"C:\Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\WAST PCR Prepop Live V2_PCR10000001.pdf"));
            
            log.Info($"Started with {searchRoot} : {searchPattern} : {outputFile} : {displayUpdateInterval} : {newFileInterval} : {mergeUpdateInterval}");

            // if the output file is already there then start with that information already in the list
            ConcurrentDictionary<string, ConcurrentBag<PcrPrintFileInformation>> list = new ConcurrentDictionary<string, ConcurrentBag<PcrPrintFileInformation>>();

            async Task ParsePcrFileAsync(string pcrFile)
            {
                var info = await PdfFileResolver.GetInformationFromFileAsync(new FileInfo(pcrFile));
                list.AddOrUpdate(info.PcrNumber, new ConcurrentBag<PcrPrintFileInformation> { info }, (pcr, currentList) =>
                {
                    currentList.Add(info);
                    return currentList;
                });
            }

            // we are only interested in files that have not yet been processed in a previously loaded batch
            PdfFileResolver.MergeExistingOutputFiles(outputFile);
            var excludedFiles = PdfFileResolver.GetProcessedFiles(outputFile);
            var interestingFiles = EnumerateFilesSafely(searchRoot, $"{searchPattern}*.pdf", excludedFiles);
            // start a new output file (sequentially numbered to avoid having to hold everything in memory when we don't need it)
            string originalOutputFile = outputFile;
            outputFile = PdfFileResolver.GenerateNextOutputFile(outputFile);
            long processed = 0;
            object lockTarget = new object();

            log.Info($"Parsing files named '{searchPattern}' from '{searchRoot}', skipping {excludedFiles.Count} already processed files");

            var startTime = DateTime.UtcNow;

            void UpdateProgressCount(long count)
            {
                if (processed % displayUpdateInterval == 0)
                {
                    log.Info($"Processed {count} files so far ({count / (DateTime.UtcNow - startTime).TotalSeconds } files per second) . . .");

                    if (count % newFileInterval == 0)
                    {
                        // Write the output then start a new file and clear out the memory currently being used by the list
                        var currentListKeys = list.Keys.ToArray();
                        var currentOutput = outputFile;
                        outputFile = PdfFileResolver.GenerateNextOutputFile(originalOutputFile);
                        File.WriteAllText(currentOutput, JsonConvert.SerializeObject(list, Formatting.None));
                        Parallel.ForEach(currentListKeys, key => list.TryRemove(key, out ConcurrentBag<PcrPrintFileInformation> value));

                        // if we have completed a suitable number of iterations we can merge the logs down
                        if (count % mergeUpdateInterval == 0)
                        {
                            
                            log.Info($"Merged output files giving a total of {PdfFileResolver.MergeExistingOutputFiles(originalOutputFile)} files processed");
                        }
                    }
                    else
                    {
                        // simply write the output
                        File.WriteAllText(outputFile, JsonConvert.SerializeObject(list, Formatting.None));
                    }
                }
            }

            //foreach (var pcrFile in interestingFiles)
            Parallel.ForEach(interestingFiles, async pcrFile =>
            {
                try
                {
                    await ParsePcrFileAsync(pcrFile);
                }
                catch (Exception x)
                {
                    log.Error($"Error processing file '{pcrFile}'", x);
                }
                finally
                {
                    lock (lockTarget)
                    {
                        UpdateProgressCount(++processed);
                    }
                }
            });

            // update the progress one last time to note the total number of files that have actually been processed
            UpdateProgressCount(processed);

            if (list.Any(kvp => kvp.Value.Count > 1))
            {
                log.Warn($"WARNING!!! We have multiple files referencing the same PCR number:");
                foreach (var item in list.Where(kvp => kvp.Value.Count > 1))
                {
                    log.Warn($"  PCR='{item.Key}', Files: {string.Join(" : ", item.Value.Select(v => $"'{v.FilePath}'"))}");
                }
            }

            if (list.Any(kvp => kvp.Value.Any(i => i.PageAddresses?.Any(p => string.IsNullOrWhiteSpace(p.PatternPageAddress)) == true)))
            {
                log.Warn($"WARNING!!! We have failed to identify the pattern address of some files:");
                foreach (var item in list.Where(kvp => kvp.Value.Any(i => i.PageAddresses?.Any(p => string.IsNullOrWhiteSpace(p.PatternPageAddress)) == true))
                    .SelectMany(kvp => kvp.Value.Where(i => i.PageAddresses?.Any(p => string.IsNullOrWhiteSpace(p.PatternPageAddress)) == true)))
                {
                    log.Warn($"  '{item.FilePath}'");
                }
            }

            log.Info("Parsing Complete writing output");

            // catch all just in case the whole thing hasn't been written yet (it should have been)
            File.WriteAllText(outputFile, JsonConvert.SerializeObject(list, Formatting.None));
            list.Clear();

            // merge the temp working file down into the file originally asked for
            PdfFileResolver.MergeExistingOutputFiles(originalOutputFile);

            log.Info($"Output written to '{outputFile}'");
        }

        private static IEnumerable<string> EnumerateFilesSafely(string path, string searchPattern, HashSet<string> excludedFiles)
        {
            foreach (var file in Directory.EnumerateFiles(path, searchPattern).Union(
                Directory.EnumerateDirectories(path).SelectMany(d =>
                {
                    try
                    {
                        return EnumerateFilesSafely(d, searchPattern, excludedFiles);
                    }
                    catch (UnauthorizedAccessException)
                    {
                        return Enumerable.Empty<string>();
                    }
                })))
            {
                if (!excludedFiles.Contains(file.ToLower()))
                {
                    yield return file;
                }
            }
        }
    }
}