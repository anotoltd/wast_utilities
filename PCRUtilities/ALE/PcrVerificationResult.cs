﻿using System;

namespace PCRUtilities.ALE
{
    public class PcrVerificationResult
    {
        public form Form { get; internal set; }
        public answer Answer { get; internal set; }
        public answer_submissions AnswerSubmission { get; internal set; }
        public form_submissions FormSubmission { get; internal set; }
    }
}