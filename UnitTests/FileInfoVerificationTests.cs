﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PCRUtilities;

namespace UnitTests
{
    [TestClass]
    public class FileInfoVerificationTests
    {
        [TestMethod]
        public void TestAllEqualPcrValues()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerPcr = "12345", AnswerSubmissionPcr = "12345", PcrNumber = "12345" };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsValid);
            Assert.AreEqual(0, result.ValidationErrors.Count);
        }

        [TestMethod]
        public void TestOneMissingPcrValues()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerPcr = "12345", AnswerSubmissionPcr = "", PcrNumber = "12345" };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsValid);
            Assert.AreEqual(0, result.ValidationErrors.Count);
        }

        [TestMethod]
        public void TestOneNullPcrValues()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerPcr = "12345", AnswerSubmissionPcr = null, PcrNumber = "12345" };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsValid);
            Assert.AreEqual(0, result.ValidationErrors.Count);
        }

        [TestMethod]
        public void TestTwoNullPcrValues()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerPcr = null, AnswerSubmissionPcr = null, PcrNumber = "12345" };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsValid);
            Assert.AreEqual(0, result.ValidationErrors.Count);
        }

        [TestMethod]
        public void TestTwoMissingOneNullPcrValues()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerPcr = "", AnswerSubmissionPcr = null, PcrNumber = "12345" };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsValid);
            Assert.AreEqual(0, result.ValidationErrors.Count);
        }

        [TestMethod]
        public void TestAllEqualBillIdValues()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerBillId = 12345, FormBillId = 12345, FormSubmissionBillId= 12345 };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsValid);
            Assert.AreEqual(0, result.ValidationErrors.Count);
        }

        [TestMethod]
        public void TestOneMissingBillIdValues()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerBillId = null, FormBillId = 12345, FormSubmissionBillId = 12345 };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsValid);
            Assert.AreEqual(0, result.ValidationErrors.Count);
        }

        [TestMethod]
        public void TestTwoMissingBillIdValues()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerBillId = null, FormBillId = null, FormSubmissionBillId = 12345 };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsValid);
            Assert.AreEqual(0, result.ValidationErrors.Count);
        }

        [TestMethod]
        public void TestAllMissingBillIdValues()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerBillId = null, FormBillId = null, FormSubmissionBillId = null };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsValid);
            Assert.AreEqual(0, result.ValidationErrors.Count);
        }

        [TestMethod]
        public void TestUnequalBillIdValues()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerBillId = 1, FormBillId = 2, FormSubmissionBillId = 3 };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsValid);
            Assert.AreEqual(1, result.ValidationErrors.Count);
        }

        [TestMethod]
        public void TestOneUnequalBillIdValue()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerBillId = 1, FormBillId = 2, FormSubmissionBillId = 2 };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsValid);
            Assert.AreEqual(1, result.ValidationErrors.Count);
        }

        [TestMethod]
        public void TestOneUnequalAndOneMissingBillIdValue()
        {
            PcrPrintFileInformation info = new PcrPrintFileInformation { AnswerBillId = 1, FormBillId = 2, FormSubmissionBillId = null };
            var result = info.Validate();
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsValid);
            Assert.AreEqual(1, result.ValidationErrors.Count);
        }
    }
}
