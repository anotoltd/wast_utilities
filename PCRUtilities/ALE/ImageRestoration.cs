﻿using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCRUtilities.ALE
{
    public class ImageRestoration
    {
        private static ILog log = LogManager.GetLogger(typeof(ImageRestoration));

        public static void RestoreImages(string inputDataPath, string dataPath)
        {
            try
            {
                using (var reader = new StreamReader(File.OpenRead(inputDataPath)))
                {
                    string capturesDir = Path.Combine(dataPath, "captures");

                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        var split = line.Split(',');
                        var file = new FileInfo(Path.Combine(capturesDir, split[1].Trim() + "_1.png"));
                        if (file.Exists)
                        {
                            var pcr = split[0].Trim();
                            log.Debug($"Adding PCR '{pcr}' to file {file.FullName}");
                            WritePcrToImage(pcr, file, backup: true);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                log.Error("Error writing PCR to image", x);
            }
        }

        //public static void RestoreImages(string dataPath, string dbInstance, string database, string username, string password, int commandTimeout)
        //{
        //    try
        //    {
        //        using (var context = ConnectionHelper.GetContext(dbInstance, database, username, password))
        //        {
        //            context.Database.CommandTimeout = commandTimeout;

        //            string capturesDir = Path.Combine(dataPath, "captures");

        //            foreach (var item in context.answers.Where(a => a.questionRef == "PCR" &&
        //            a.recordCreatedDate_utc > new DateTime(2018, 7, 23)).Select(a => new { a.form.FormUnique, Pcr = a.description }))
        //            {
        //                var file = new FileInfo(Path.Combine(capturesDir, item.FormUnique + "_1.png"));
        //                if (file.Exists)
        //                {
        //                    log.Debug($"Adding PCR '{item.Pcr}' to file {file.FullName}");
        //                    WritePcrToImage(item.Pcr, file, backup: true);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        log.Error("Error writing PCR to image", x);
        //    }
        //}

        internal static void WritePcrToImage(string pcrNumber, FileInfo sourceFile, bool backup = true)
        {
            if (!string.IsNullOrWhiteSpace(pcrNumber))
            {
                if (backup)
                {
                    string backupPath = Path.Combine(sourceFile.DirectoryName, $"{Path.GetFileNameWithoutExtension(sourceFile.Name)}_backup{sourceFile.Extension}");
                    if (!File.Exists(backupPath))
                    {
                        File.Copy(sourceFile.FullName, backupPath);
                    }
                }

                string destFileName = sourceFile.FullName + ".modified";

                using (Bitmap bmp = new Bitmap(sourceFile.FullName))
                {
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        float fontSize = 12f;
                        g.DrawString(pcrNumber, new Font("Arial", fontSize), new SolidBrush(Color.Black), new PointF(20, 36));
                        bmp.Save(destFileName, ImageFormat.Png);
                    }
                }

                sourceFile.Delete();
                File.Move(destFileName, sourceFile.FullName);
            }
        }

        //internal static void RestoreImages(string destinationDbInstance, string destinationDb, string username, string password)
        //{
        //    PCRUtilities.ALE.ImageRestoration.RestoreImages("DataRecovery", destinationDbInstance, destinationDb, username, password);
        //}
    }
}