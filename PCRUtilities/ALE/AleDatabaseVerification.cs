﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCRUtilities.ALE
{
    public class AleDatabaseVerification : IDisposable
    {
        internal readonly AleEntities EntityContext;
        private readonly static ILog log = LogManager.GetLogger(typeof(AleDatabaseVerification));

        public AleDatabaseVerification(string instance, string database, string username, string password)
        {
            EntityContext = ConnectionHelper.GetContext(instance, database, username, password);
            EntityContext.Configuration.AutoDetectChangesEnabled = false;
        }

        public void AddResult(PcrVerificationResult result)
        {
            if (result.Form != null)
                EntityContext.forms.Add(result.Form);
            if (result.FormSubmission != null)
                EntityContext.form_submissions.Add(result.FormSubmission);
            if (result.Answer != null)
                EntityContext.answers.Add(result.Answer);
            if (result.AnswerSubmission != null)
                EntityContext.answer_submissions.Add(result.AnswerSubmission);
        }

        public async Task AcceptChanges()
        {
            await EntityContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            EntityContext.Dispose();
        }

        public static void ConsolidateDataRecords(string indexFile, string destinationDbInstance, string destinationDb, string username, string password, string outputFile)
        {
            log.Info($"Started consolidation of records from '{indexFile}' against '{destinationDbInstance}\\{destinationDb}'");
            // map the data in the index file to pattern address
            ConcurrentDictionary<long, PcrPrintFileInformation> patternDic = new ConcurrentDictionary<long, PcrPrintFileInformation>();
            Parallel.ForEach(DataFileUtilities.ReadDataFile(indexFile), fileInfo =>
            {
                patternDic.TryAdd((long)fileInfo.PageAddresses.First(pa => pa.PageNumber == 1).PageAddressValue, fileInfo);
            });

            log.Info($"Loaded {patternDic.Count} pattern ranges for comparison");

            using (var verifier = new AleDatabaseVerification(destinationDbInstance, destinationDb, username, password))
            {
                log.Info("Started matching against the forms table");
                // look through all the forms and map the bill id to the correct print file
                foreach (var item in verifier.EntityContext.forms.AsNoTracking()
                                                                 .Where(f => f.appId == 7 && f.uniqueStartAddressId != null)
                                                                 .Select(f => new { f.billId, f.uniqueStartAddressId }))
                {
                    if (patternDic.TryGetValue(item.uniqueStartAddressId.Value, out PcrPrintFileInformation fileInfo))
                    {
                        fileInfo.FormBillId = item.billId;
                        log.Debug($"Matched pattern address {item.uniqueStartAddressId} to Bill Id {fileInfo.FormBillId}");
                    }
                }
            }

            using (var verifier = new AleDatabaseVerification(destinationDbInstance, destinationDb, username, password))
            {
                log.Info("Started matching against the form_submissions table");
                // look through all the form_submissions and map the bill id to the correct print file
                foreach (var item in verifier.EntityContext.form_submissions.AsNoTracking()
                                                                            .Where(f => f.appId == 7 && f.uniquestartAddressId != null)
                                                                            .Select(f => new { f.billId, f.uniquestartAddressId }))
                {
                    if (patternDic.TryGetValue(item.uniquestartAddressId.Value, out PcrPrintFileInformation fileInfo))
                    {
                        fileInfo.FormSubmissionBillId = item.billId;
                        log.Debug($"Matched pattern address {item.uniquestartAddressId} to Bill Id {fileInfo.FormSubmissionBillId}");
                    }
                }
            }

            using (var verifier = new AleDatabaseVerification(destinationDbInstance, destinationDb, username, password))
            {
                log.Info("Started matching against the answers table");
                // look through all the answers and map the bill id to the correct print file along with the recorded PCR Number
                foreach (var item in verifier.EntityContext.answers.AsNoTracking()
                                                                   .Where(a => a.form.appId == 7 && a.questionRef.ToUpper() == "PCR")
                                                                   .Select(a => new { a.billId, PcrNumber = a.description, a.form.uniqueStartAddressId }))
                {
                    if (patternDic.TryGetValue(item.uniqueStartAddressId.Value, out PcrPrintFileInformation fileInfo))
                    {
                        fileInfo.AnswerBillId = item.billId;
                        fileInfo.AnswerPcr = item.PcrNumber;
                        log.Debug($"Matched pattern address {item.uniqueStartAddressId} to Bill Id {fileInfo.AnswerBillId} and PCR number {fileInfo.AnswerPcr}");
                    }
                }
            }

            using (var verifier = new AleDatabaseVerification(destinationDbInstance, destinationDb, username, password))
            {
                log.Info("Started matching against the answer_submissions table");
                // look through all the answers and map the recorded PCR Number to the correct print file 
                foreach (var item in verifier.EntityContext.answer_submissions.AsNoTracking()
                                                                              .Where(a => a.form_submission.appId == 7 && a.questionRef.ToUpper() == "PCR")
                                                                              .Select(a => new { PcrNumber = a.description, a.form_submission.uniquestartAddressId }))
                {
                    if (patternDic.TryGetValue(item.uniquestartAddressId.Value, out PcrPrintFileInformation fileInfo))
                    {
                        fileInfo.AnswerSubmissionPcr = item.PcrNumber;
                        log.Debug($"Matched pattern address {item.uniquestartAddressId} to PCR number {fileInfo.AnswerSubmissionPcr}");
                    }
                }
            }

            // write the results to file
            patternDic.StreamToJson(outputFile, itemTranslator: kvp => kvp.Value);
            log.Info($"Written output to file '{outputFile}'");

            DateTime start = DateTime.UtcNow;
            int count = 0;

            log.Info($"Starting to input missing data");
            foreach (var item in patternDic)
            {
                PcrPrintFileInfoValidationResponse validationResponse = item.Value.Validate();
                if (validationResponse.IsValid && !item.Value.IsCompletelyMapped)
                {
                    try
                    {
                        using (var verifier = new AleDatabaseVerification(destinationDbInstance, destinationDb, username, password))
                        {
                            int? billId = item.Value.GetBillId();

                            form form = billId.HasValue ? verifier.EntityContext.forms.Find(billId) : null;
                            form_submissions formSubmission = billId.HasValue ?
                                verifier.EntityContext.form_submissions
                                                      .AsNoTracking()
                                                      .FirstOrDefault(fs => fs.billId == billId &&
                                                        (fs.penTransport == "Data Print" || fs.penTransport == "DataRecovery" || fs.penTransport == "DataRecovery_FormCopy"))
                                                      : null;

                            if (form == null)
                            {
                                form = AddForm(item.Value, formSubmission);
                                verifier.EntityContext.forms.Add(form);
                                log.Debug($"Added form for PCR number {item.Value.PcrNumber}");
                            }

                            if (formSubmission == null)
                            {
                                formSubmission = AddFormSubmission(form, item.Value);
                                verifier.EntityContext.form_submissions.Add(formSubmission);
                                log.Debug($"Added form_submission for PCR number {item.Value.PcrNumber}");
                            }

                            if (item.Value.AnswerBillId == null)
                            {
                                verifier.EntityContext.answers.Add(AddAnswer(form, item.Value));
                                log.Debug($"Added answer for PCR number {item.Value.PcrNumber}");
                            }

                            if (string.IsNullOrWhiteSpace(item.Value.AnswerSubmissionPcr))
                            {
                                verifier.EntityContext.answer_submissions.Add(AddAnswerSubmission(formSubmission, item.Value));
                                log.Debug($"Added answer_submission for PCR number {item.Value.PcrNumber}");
                            }

                            if (verifier.EntityContext.ChangeTracker.HasChanges())
                            {
                                verifier.EntityContext.SaveChanges();
                                log.Debug($"Records saved to the database successfully for PCR number {item.Value.PcrNumber}");
                            }
                        }
                    }
                    catch (Exception x)
                    {
                        log.Fatal("Error occured while merging data to database", x);
                        log.Warn(JsonConvert.SerializeObject(item.Value, Formatting.Indented));
                        // in this case we want to rethrow because we don't want to continue where we might have missed an entry
                        throw;
                    }
                }
                else if (!validationResponse.IsValid)
                {
                    log.Error($"Invalid mapping information found for file '{item.Value.FilePath}'");
                    foreach (string error in validationResponse.ValidationErrors)
                    {
                        log.Warn(error);
                    }
                }

                if (++count % 10000 == 0)
                {
                    log.Info($"Parsed {count} files so far at a rate of {count / (DateTime.UtcNow - start).TotalSeconds} files per second");
                }
            }

            log.Info($"Consolidation of {count} records completed");
        }

        public PcrVerificationResult VerifyPcrEntry(PcrPrintFileInformation fileInfo)
        {
            PcrVerificationResult result = new PcrVerificationResult();

            var startPage = fileInfo.PageAddresses.First(p => p.PageNumber == 1);
            var formMatch = EntityContext.forms.AsNoTracking()
                                               .Where(f => f.appId == 7 && f.uniqueStartAddressId == (long)startPage.PageAddressValue).FirstOrDefault();
            var matchingAnswer = EntityContext.answers.AsNoTracking()
                                                      .Where(a => a.questionRef.ToUpper() == "PCR" && a.form.appId == 7 && a.description == fileInfo.PcrNumber);
            var matchingAnswerSubmission = EntityContext.answer_submissions.AsNoTracking()
                                                                           .Where(a => a.questionRef.ToUpper() == "PCR" && a.form_submission.appId == 7 && a.description == fileInfo.PcrNumber);

            form_submissions formSubmissionMatch = null;
            answer answerMatch = null;
            answer_submissions answerSubmissionMatch = null;

            bool FormSubmissionMatchExists()
            {
                if (formSubmissionMatch == null)
                {
                    // look up the billid directly then if it exists we can pull in the whole record
                    var matchingBillId = EntityContext.form_submissions.AsNoTracking()
                                                                       .Where(f => f.appId == 7 && f.uniquestartAddressId == (long)startPage.PageAddressValue)
                                                                       .Select(f => f.billId).SingleOrDefault();
                    if (matchingBillId > 0)
                    {
                        formSubmissionMatch = EntityContext.form_submissions.Find(matchingBillId);
                    }
                }

                return formSubmissionMatch != null;
            }

            bool AnswerMatchExists()
            {
                if (answerMatch == null)
                {
                    answerMatch = matchingAnswer.FirstOrDefault();
                }

                return answerMatch != null;
            }

            bool AnswerSubmissionMatchExists()
            {
                if (answerSubmissionMatch == null)
                {
                    answerSubmissionMatch = matchingAnswerSubmission.FirstOrDefault();
                }

                return answerSubmissionMatch != null;
            }

            // do I have a form on the correct pattern?
            if (formMatch != null)
            {
                // do I have a form_submission on the correct pattern?
                if (FormSubmissionMatchExists())
                {
                    // do the bill id's match in the forms and form_submissions matches
                    if (formMatch.billId != formSubmissionMatch.billId)
                    {
                        log.Fatal($"There is an entry in the form table for pattern '{PageAddress.GetPageAddressFromValue((ulong)formMatch.uniqueStartAddressId)}' " +
                            $"with bill ID '{formMatch.billId}' however the entry in the form_submissions table for the same pattern has the bill ID " +
                            $"'{formSubmissionMatch.billId}'");
                        throw new NotSupportedException("Bill ID for form and form_submission did not match");
                    }

                    // do I have and entry in the answers table for this PCR number?
                    if (AnswerMatchExists())
                    {
                        // is the PCR entry for the correct form
                        if (answerMatch.billId != formMatch.billId)
                        {
                            log.Fatal($"There is an entry in the answers table for PCR '{fileInfo.PcrNumber}' which is defined against pattern " +
                                $"'{PageAddress.GetPageAddressFromValue((ulong)answerMatch.form.uniqueStartAddressId)}' " +
                                $"however the print asset was generated against pattern '{startPage.PatternPageAddress}'");
                            throw new NotSupportedException("Bill ID for answer and form did not match");
                        }

                        // do I have and entry in the answer_submissions table for this PCR number?
                        if (AnswerSubmissionMatchExists())
                        {
                            // all is as it should be :)
                            log.Debug($"Found existing entry for PCR {fileInfo.PcrNumber} in database with bill ID {answerMatch.billId}");
                        }
                        else
                        {
                            // we have everything other than the answer_submission entry
                            result.AnswerSubmission = AddAnswerSubmission(formSubmissionMatch, fileInfo);
                        }
                    }
                    else
                    {
                        // do I have and entry in the answer_submissions table for this PCR number?
                        if (AnswerSubmissionMatchExists())
                        {
                            // we need to add the answer
                            result.Answer = AddAnswer(formMatch, fileInfo);
                        }
                        else
                        {
                            // we need both answer and answer_submission entries
                            result.Answer = AddAnswer(formMatch, fileInfo);
                            result.AnswerSubmission = AddAnswerSubmission(formSubmissionMatch, fileInfo);
                        }
                    }
                }
                else
                {
                    // add the form_submission entry
                    formSubmissionMatch = AddFormSubmission(formMatch, fileInfo);
                    result.FormSubmission = formSubmissionMatch;

                    // do I have and entry in the answers table for this PCR number?
                    if (AnswerMatchExists())
                    {
                        // is the PCR entry for the correct form
                        if (answerMatch.billId != formMatch.billId)
                        {
                            log.Fatal($"There is an entry in the answers table for PCR '{fileInfo.PcrNumber}' which is defined against pattern " +
                                $"'{PageAddress.GetPageAddressFromValue((ulong)answerMatch.form.uniqueStartAddressId)}' " +
                                $"however the print asset was generated against pattern '{startPage.PatternPageAddress}'");
                            throw new NotSupportedException("Bill ID for answer and form did not match");
                        }
                    }
                    else
                    {
                        // we need to add the answer
                        result.Answer = AddAnswer(formMatch, fileInfo);
                    }

                    // do I have and entry in the answer_submissions table for this PCR number?
                    if (AnswerSubmissionMatchExists())
                    {
                        log.Fatal($"There is an entry in the answer_submissions table for PCR '{fileInfo.PcrNumber}' which is defined against pattern " +
                            $"'{PageAddress.GetPageAddressFromValue((ulong)answerSubmissionMatch.form_submission.uniquestartAddressId)}' " +
                            $"however the print asset was generated against pattern '{startPage.PatternPageAddress}'. " +
                            $"The Bill ID for the answer_submission entry is {answerSubmissionMatch.form_submission.billId}");
                        throw new NotSupportedException("Pattern mismatch between for answer_submission and form_submission. Form_submission not found");
                    }
                    else
                    {
                        result.AnswerSubmission = AddAnswerSubmission(formSubmissionMatch, fileInfo);
                    }
                }
            }
            else
            {
                // do I have a form_submission on the correct pattern?
                if (FormSubmissionMatchExists())
                {
                    // add the form to the database using the existing information from form_submissions
                    formMatch = AddForm(fileInfo, formSubmissionMatch);
                    result.Form = formMatch;
                }
                else
                {
                    // add the form and the form_submission to the database
                    formMatch = AddForm(fileInfo);
                    result.Form = formMatch;
                    formSubmissionMatch = AddFormSubmission(formMatch, fileInfo);
                    result.FormSubmission = formSubmissionMatch;
                }

                // do I have and entry in the answers table for this PCR number?
                if (AnswerMatchExists())
                {
                    log.Fatal($"There is an entry in the answers table for PCR '{fileInfo.PcrNumber}' which is defined against pattern " +
                        $"'{PageAddress.GetPageAddressFromValue((ulong)answerMatch.form.uniqueStartAddressId)}' however the print asset was generated against pattern " +
                        $"'{startPage.PatternPageAddress}'");
                    throw new NotSupportedException("Answer exists in database but the form entry does not map it to the correct pattern range");
                }
                else
                {
                    // we need to add the answer
                    result.Answer = AddAnswer(formMatch, fileInfo);
                }

                // do I have and entry in the answer_submissions table for this PCR number?
                if (AnswerSubmissionMatchExists())
                {
                    // do I have a corresponding form_submission match?
                    if (FormSubmissionMatchExists() && formSubmissionMatch.form_submissionId != answerSubmissionMatch.form_submissionId)
                    {
                        log.Fatal($"There is an entry in the answer_submission table for PCR '{fileInfo.PcrNumber}' which is defined against pattern " +
                            $"'{PageAddress.GetPageAddressFromValue((ulong)answerSubmissionMatch.form_submission.uniquestartAddressId)}' " +
                            $"however the print asset was generated against pattern '{startPage.PatternPageAddress}'");
                        throw new NotSupportedException("Answer exists in database but the form entry does not map it to the correct pattern range");
                    }
                }
                else
                {
                    // need to add the answer_submission to the database
                    result.AnswerSubmission = AddAnswerSubmission(formSubmissionMatch, fileInfo);
                }
            }

            return result;
        }

        private static form AddForm(PcrPrintFileInformation fileInfo, form_submissions formSubmissionMatch = null)
        {
            var page1Info = fileInfo.PageAddresses.First(pa => pa.PageNumber == 1);
            var startDate = DateTime.UtcNow;
            form f = new form
            {
                answers = new List<answer>(),
                appId = formSubmissionMatch?.appId ?? 7,
                cms_userId = formSubmissionMatch?.cms_userId ?? 1,
                penId = formSubmissionMatch?.penId ?? -1,
                received_timestamp_utc = formSubmissionMatch?.received_timestamp_utc ?? startDate,
                penOwnerName = formSubmissionMatch?.penOwnerName ?? "henry.carter@anoto.com",
                penOwnerEmail = formSubmissionMatch?.penOwnerEmail ?? "henry.carter@anoto.com",
                penTransport = formSubmissionMatch == null ? "DataRecovery" : "DataRecovery_FormSubmissionCopy",
                start_timestamp_utc = formSubmissionMatch?.start_timestamp_utc ?? startDate,
                end_timestamp_utc = formSubmissionMatch?.end_timestamp_utc ?? startDate,
                recordCreatedDate_utc = startDate,
                uniqueStartAddressId = formSubmissionMatch?.uniquestartAddressId ?? (long)page1Info.PageAddressValue,
                FormUnique = formSubmissionMatch?.FormUnique ?? $@"{startDate.ToString("yyyyMMdd")}\henryc_{page1Info.PatternPageAddress}",
                process_start_utc = formSubmissionMatch?.process_start_utc ?? startDate,
                magicBoxPageAddress = string.Empty,
                size = 0,
                formFields = 0,
                filledFields = 0,
                extraStrokes = 0,
                latitude = 0,
                longitude = 0,
                invalidReason = "false",
                version = 0,
                penOwnerMobile = string.Empty,
                pgcFileName = string.Empty
            };

            if (formSubmissionMatch != null)
            {
                // TODO: might want to copy other properties across at this point 
                f.billId = formSubmissionMatch.billId;
                f.form_submissions.Add(formSubmissionMatch);
            }

            //entityContext.forms.Add(f);
            log.Debug($"Added form for PCR {fileInfo.PcrNumber}");
            return f;
        }

        private static form_submissions AddFormSubmission(form formMatch, PcrPrintFileInformation fileInfo)
        {
            if (formMatch == null)
            {
                throw new ArgumentNullException(nameof(formMatch));
            }

            form_submissions fs = new form_submissions
            {
                appId = formMatch.appId,
                form = formMatch,
                billId = formMatch.billId,
                cms_userId = formMatch.cms_userId,
                end_timestamp_utc = formMatch.end_timestamp_utc,
                FormUnique = formMatch.FormUnique,
                penId = formMatch.penId,
                penOwnerEmail = formMatch.penOwnerEmail,
                penOwnerName = formMatch.penOwnerName,
                penTransport = "DataRecovery_FormCopy",
                process_start_utc = formMatch.process_start_utc,
                received_timestamp_utc = formMatch.received_timestamp_utc,
                start_timestamp_utc = formMatch.start_timestamp_utc,
                uniquestartAddressId = formMatch.uniqueStartAddressId,
                magicBoxPageAddress = string.Empty,
                size = 0,
                formFields = 0,
                filledFields = 0,
                extraStrokes = 0,
                latitude = 0,
                longitude = 0,
                invalidReason = "false"
            };

            //entityContext.form_submissions.Add(fs);
            log.Debug($"Added form_submission for PCR {fileInfo.PcrNumber}");
            return fs;
        }

        private static answer AddAnswer(form formMatch, PcrPrintFileInformation fileInfo)
        {
            if (formMatch == null)
            {
                throw new ArgumentNullException(nameof(formMatch));
            }

            DateTime time = DateTime.UtcNow;
            answer a = new answer
            {
                answerId = 1,
                form = formMatch,
                billId = formMatch.billId,
                description = fileInfo.PcrNumber,
                end_timestamp_utc = time,
                filter = "last",
                questionId = 1,
                questionRef = "PCR",
                readOnly = true,
                recognitionScore = 1,
                recordCreatedDate_utc = time,
                start_timestamp_utc = time,
                timestamp = time,
                hwrType = string.Empty
            };

            //entityContext.answers.Add(a);
            log.Debug($"Added answer for PCR {fileInfo.PcrNumber}");
            return a;
        }

        private static answer_submissions AddAnswerSubmission(form_submissions formSubmissionMatch, PcrPrintFileInformation fileInfo)
        {
            if (formSubmissionMatch == null)
            {
                throw new ArgumentNullException(nameof(formSubmissionMatch));
            }

            DateTime time = DateTime.UtcNow;
            answer_submissions a = new answer_submissions
            {
                answerId = 1,
                description = fileInfo.PcrNumber,
                end_timestamp_utc = time,
                filter = "last",
                form_submission = formSubmissionMatch,
                form_submissionId = formSubmissionMatch.form_submissionId,
                questionId = 1,
                questionRef = "PCR",
                recognitionScore = 1,
                recordCreatedDate_utc = time,
                start_timestamp_utc = time,
                timestamp = time,
                hwrType = string.Empty
            };

            //entityContext.answer_submissions.Add(a);
            log.Debug($"Added answer_submission for PCR {fileInfo.PcrNumber}");
            return a;
        }
    }
}
