﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCRUtilities
{
    public static class DataFileUtilities
    {
        public static IEnumerable<PcrPrintFileInformation> ReadDataFile(string indexFile)
        {
            using (var reader = new JsonTextReader(File.OpenText(indexFile)))
            {
                var serialiser = JsonSerializer.Create(new JsonSerializerSettings { });
                // read off the first item (start object for the dictionary)
                reader.Read();
                while (reader.Read())
                {
                    if (reader.TokenType == JsonToken.StartObject)
                    {
                        yield return serialiser.Deserialize<PcrPrintFileInformation>(reader);
                    }
                }
            }
        }

        public static void StreamToJson<T>(this IEnumerable<T> enumerable, string file, Func<T, object> itemTranslator = null)
        {
            using (var writer = new JsonTextWriter(File.CreateText(file)))
            {
                writer.WriteStartArray();

                var serialiser = JsonSerializer.Create(new JsonSerializerSettings { });
                foreach (var item in enumerable)
                {
                    serialiser.Serialize(writer, itemTranslator == null ? item : itemTranslator(item));
                }

                writer.WriteEndArray();
            }
        }

        public static void StreamToJson<TValue>(this IDictionary<string, TValue> dictionary, string file)
        {
            using (var writer = new JsonTextWriter(File.CreateText(file)))
            {
                writer.WriteStartObject();

                var serialiser = JsonSerializer.Create(new JsonSerializerSettings { });
                foreach (var item in dictionary)
                {
                    writer.WritePropertyName(item.Key);

                    serialiser.Serialize(writer, item.Value);
                }

                writer.WriteEndObject();
                writer.Flush();
            }
        }
    }
}
