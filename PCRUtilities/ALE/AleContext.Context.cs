﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PCRUtilities.ALE
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    public partial class AleEntities : DbContext
    {
        public AleEntities() : this("name=AleEntities") { }

        public AleEntities(string nameOrConnectionString) : base(nameOrConnectionString) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }

        public virtual DbSet<answer_submissions> answer_submissions { get; set; }
        public virtual DbSet<answer> answers { get; set; }
        public virtual DbSet<application> applications { get; set; }
        public virtual DbSet<cms_users> cms_users { get; set; }
        public virtual DbSet<form_submissions> form_submissions { get; set; }
        public virtual DbSet<form> forms { get; set; }
    }
}
