﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using IO = System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using log4net;

namespace PCRUtilities
{
    public class PdfFileResolver
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PdfFileResolver));

        public static async Task<PcrPrintFileInformation> GetInformationFromFileAsync(FileInfo file)
        {
            var retVal = new PcrPrintFileInformation { FilePath = file.FullName };

            retVal.PcrNumber = GetPcrFromFile(file);

            retVal.PageAddresses = await GetPageAddressesFromFileAsync(file);

            return retVal;
        }

        private static async Task<PageAddress[]> GetPageAddressesFromFileAsync(FileInfo file)
        {
            List<PageAddress> pages = new List<PageAddress>();

            using (var fs = file.OpenRead())
            {
                using (PdfReader reader = new PdfReader(fs))
                {
                    for (int page = 1; page <= reader.NumberOfPages; page++)
                    {
                        var pageAddress = new PageAddress { PageNumber = page };
                        pages.Add(pageAddress);

                        using (TextReader txtReader = new StreamReader(new MemoryStream(reader.GetPageContent(page))))
                        {
                            string line;
                            while ((line = await txtReader.ReadLineAsync()) != null)
                            {
                                var slIndex = line.IndexOf(" - SL");
                                if (slIndex > 0)
                                {
                                    string patternAddress = line.Substring(0, slIndex).Split((char)10).Last().TrimStart('(').Trim();
                                    pageAddress.SetPageAddress(patternAddress);
                                    break;
                                }
                            }
                        }


                        //ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        //string currentText = PdfTextExtractor.GetTextFromPage(reader, page, strategy);

                        //currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText)));

                        //var endOfPattern = currentText.IndexOf(" - SL\n");
                        //if (endOfPattern > 0)
                        //{
                        //    string patternAddress = currentText.Substring(0, endOfPattern).Split((char)10).Last();
                        //    pageAddress.SetPageAddress(patternAddress);
                        //}
                    }
                }
            }

            return pages.ToArray();
        }

        public static int MergeExistingOutputFiles(string outputFile)
        {
            if (File.Exists(outputFile))
            {
                FileInfo outFileInfo = new FileInfo(outputFile);

                ConcurrentDictionary<string, List<PcrPrintFileInformation>> fullResults = new ConcurrentDictionary<string, List<PcrPrintFileInformation>>();
                Parallel.ForEach(DataFileUtilities.ReadDataFile(outputFile), fileInfo =>
                {
                    fullResults.AddOrUpdate(fileInfo.PcrNumber, new List<PcrPrintFileInformation>(new PcrPrintFileInformation[] { fileInfo }), (pcr, list) =>
                    {
                        list.Add(fileInfo);
                        return list;
                    });
                });

                log.Debug($"Merging from a starting point of {fullResults.Count}");

                foreach (var file in outFileInfo.Directory.EnumerateFiles($"{IO.Path.GetFileNameWithoutExtension(outputFile)}_*{outFileInfo.Extension}", SearchOption.TopDirectoryOnly))
                {
                    foreach (var item in DataFileUtilities.ReadDataFile(file.FullName))
                    {
                        fullResults.AddOrUpdate(item.PcrNumber, new List<PcrPrintFileInformation> { item }, (pcr, list) =>
                        {
                            // de-dupe as a fail safe
                            foreach (var dupeGroup in list.GroupBy(i => i.FilePath.ToLower()).Where(g => g.Count() > 1))
                            {
                                for (int i = 1; i < dupeGroup.Count(); i++)
                                {
                                    list.Remove(dupeGroup.ElementAt(i));
                                }
                            }
                            if (!list.Any(i => i.FilePath.ToLower() == item.FilePath.ToLower()))
                            {
                                list.Add(item);
                            }
                            return list;
                        });
                    }
                    file.Delete();
                }

                RemoveInformationProcessedFromBackupFolder(fullResults);

                var backupFileName = GenerateNextOutputFile(IO.Path.Combine(outFileInfo.DirectoryName, "Backups",
                    $"{IO.Path.GetFileNameWithoutExtension(outputFile)}-({DateTime.UtcNow.ToString("yyyyMMdd-HH.mm")}){outFileInfo.Extension}"));
                Directory.CreateDirectory(IO.Path.GetDirectoryName(backupFileName));
                File.Copy(outputFile, backupFileName);
                log.Debug($"Merging {fullResults.Count} file results to disc");
                fullResults.StreamToJson(outputFile);
                //fullResults.ToDictionary(kvp => kvp.Key, kvp => kvp.Value).StreamToJson(outputFile);
                //File.WriteAllText(outputFile, JsonConvert.SerializeObject(fullResults));
                return fullResults.Select(f => f.Value.Count).Sum();
            }
            else
            {
                return 0;
            }
        }

        private static void RemoveInformationProcessedFromBackupFolder(IDictionary<string, List<PcrPrintFileInformation>> fullResults)
        {
            List<string> keysToRemove = new List<string>();
            foreach (var item in fullResults)
            {
                item.Value.RemoveAll(i => i.FilePath.Contains(@"\WAST BU\"));
                if (!item.Value.Any())
                {
                    keysToRemove.Add(item.Key);
                }
            }

            foreach (var key in keysToRemove)
            {
                fullResults.Remove(key);
            }
        }

        public static HashSet<string> GetProcessedFiles(string outputFile)
        {
            HashSet<string> retVal = new HashSet<string>();

            if (File.Exists(outputFile))
            {
                foreach (var fileInfo in DataFileUtilities.ReadDataFile(outputFile))
                {
                    retVal.Add(fileInfo.FilePath.ToLower());
                }
            }
            return retVal;
        }

        public static string GenerateNextOutputFile(string outputFile)
        {
            int index = 0;
            string original = outputFile;
            while (File.Exists(outputFile))
            {
                outputFile = IO.Path.Combine(IO.Path.GetDirectoryName(original),
                    $"{IO.Path.GetFileNameWithoutExtension(original)}_{++index}{IO.Path.GetExtension(original)}");
            }
            return outputFile;
        }

        private static string GetPcrFromFile(FileInfo file)
        {
            string pcr = null;
            if (file.Name.ToUpper().Contains("_PCR"))
            {
                var start = file.Name.ToUpper().LastIndexOf("_PCR") + 4;
                pcr = file.Name.Substring(start, file.Name.LastIndexOf('.') - start);
            }

            return pcr;
        }
    }
}
