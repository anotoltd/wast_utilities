﻿using log4net;
using Newtonsoft.Json;
using PCRUtilities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PcrPrintFileTrawler
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            string argsRepresentation = args?.Length < 0 ? "no args" : string.Join(", ", args.Select(s => $"'{s}'"));
            log.Debug($"Program started with: {argsRepresentation}");

#if DEBUG
            args = args.Pad(1, "match_data");
#endif

            string action = args?.FirstOrDefault()?.ToLower();
            if (action == "ingest")
            {
                args = args.Pad(6);
                DataFileParser.IngestDataFile(indexFile: args[1] ?? @"C:\Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\PcrIndex.json",
                    destinationDbInstance: args[2] ?? "hqintlivedev01",
                    destinationDb: args[3] ?? "HJVCTest",
                    username: args[4],
                    password: args[5]);
            }
            else if (action == "prioritise")
            {
                args = args.Pad(4);
                DataFileParser.SelectSpecificRecords(indexFile: args[1] ?? @"C:\Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\PcrIndex.json",
                    requiredPatternAddresses: args[2] ?? @"C: \Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\RequiredPatternAddresses.json",
                    outputFile: args[3] ?? @"C: \Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\RequiredPcrNumbers.json");
            }
            else if (action == "match_data")
            {
                args = args.Pad(7);
                PCRUtilities.ALE.AleDatabaseVerification.ConsolidateDataRecords(indexFile: args[1] ?? @"C:\Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\PcrIndex.json",
                    destinationDbInstance: args[2] ?? "hqintlivedev01",
                    destinationDb: args[3] ?? "HJVCTest",
                    outputFile: args[4] ?? @"C:\Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\ConsolidatedPcrIndex.json",
                    username: args[5],
                    password: args[6]);
            }
            else if (action == "extract_pcr")
            {
                args = args.Pad(3);
                DataFileParser.ExtractSpecificPcrs(indexFile: args[1] ?? @"C: \Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\RequiredPcrNumbers.json")
                    .StreamToJson(args[2] ?? @"C: \Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\RequiredPcrNumbersOnly.json");

            }
            else if (action == "image_restore")
            {
                args = args.Pad(3);
                PCRUtilities.ALE.ImageRestoration.RestoreImages(args[1] ?? @"c:\pathToInputFile.csv", args[2] ?? @"c:\data");

                //args = args.Pad(7);
                //PCRUtilities.ALE.ImageRestoration.RestoreImages(args[1] ?? @"c:\data",
                //    dbInstance: args[2] ?? "hqintlivedev01",
                //    database: args[3] ?? "HJVCTest",
                //    username: args[4],
                //    password: args[5], 
                //    commandTimeout: args[6] == null ? 300 : int.Parse(args[6]));

                //ImageProcessor.WritePcrToImage(pcrNumber: "123456",
                //    sourceFile: @"C:\Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\AJX-AA3-UQ5-H9_56.1403.118.22_1.png",
                //    destinationFile: @"C:\Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\AJX-AA3-UQ5-H9_56.1403.118.22_1_modified.png");
            }
            else
            {
                args = args.Pad(6);
                string searchRoot = args[0] ?? @"E:";
                string searchPattern = args[1] ?? "WAST PCR Prepop Live V2";
                string outputFile = args[2] ?? @"C:\Users\henryc\Documents\Anoto\Customers\WAST\PcrPrintIssue\PcrIndex.json";
                int displayUpdateInterval = int.Parse(args[3] ?? "100");
                int newFileInterval = int.Parse(args[4] ?? "1000");
                int mergeUpdateInterval = int.Parse(args[5] ?? "10000");

                //PdfFileParser.ParsePDFFilesAsync(searchRoot, searchPattern, outputFile, displayUpdateInterval, newFileInterval, mergeUpdateInterval).GetAwaiter().GetResult();
                PdfFileParser.ParsePDFFilesAsync(searchRoot, searchPattern, outputFile, displayUpdateInterval, newFileInterval, mergeUpdateInterval);
            }

            Console.WriteLine("Press enter to exit the application");
            Console.ReadLine();
        }
    }

    internal static class ArgExtensions
    {
        public static string[] Pad(this string[] args, int padTo, string padValue = null)
        {
            if (args?.Length < padTo)
            {
                if (args == null)
                {
                    args = new string[padTo];
                }
                else
                {
                    // pad the existing arguments to the correct length
                    List<string> tmp = new List<string>(args);
                    while (tmp.Count < padTo)
                    {
                        tmp.Add(padValue);
                    }
                    args = tmp.ToArray();
                }
            }

            return args;
        }
    }
}
