﻿using System;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

namespace PCRUtilities.ALE
{
    internal class ConnectionHelper
    {
        internal static AleEntities GetContext(string instance, string database, string username, string password)
        {
            var connectionString = new SqlConnectionStringBuilder
            {
                DataSource = instance,
                InitialCatalog = database,
                IntegratedSecurity = username == null, // if no username supplied then we will try to use integrated security
                ApplicationName = "EntityFramework",
                MultipleActiveResultSets = true
            };

            if (!string.IsNullOrWhiteSpace(username))
            {
                connectionString.UserID = username;
            }

            if (!string.IsNullOrWhiteSpace(password))
            {
                connectionString.Password = password;
            }

            //Build an entity framework connection string
            EntityConnectionStringBuilder entityString = new EntityConnectionStringBuilder()
            {
                Provider = "System.Data.SqlClient",
                Metadata = "res://*/ALE.AleContext.csdl|res://*/ALE.AleContext.ssdl|res://*/ALE.AleContext.msl",
                ProviderConnectionString = connectionString.ToString()
            };

            return new AleEntities(entityString.ToString());
        }
    }
}