﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using PCRUtilities;
using PCRUtilities.ALE;
using log4net;
using System.Collections.Concurrent;

namespace PcrPrintFileTrawler
{
    internal class DataFileParser
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DataFileParser));
        private static readonly string progressFileName = "progress.json";

        internal static async void IngestDataFile(string indexFile, string destinationDbInstance, string destinationDb, string username, string password)
        {
            DateTime start = DateTime.UtcNow;
            HashSet<string> pcrNumbers = File.Exists(progressFileName) ? JsonConvert.DeserializeObject<HashSet<string>>(File.ReadAllText(progressFileName)) : new HashSet<string>();

            // If changing to use the parallel.foreach you need to move the using statement inside the foreach delegate
            using (var verifier = new AleDatabaseVerification(destinationDbInstance, destinationDb, username, password))
            {

                foreach (var fileInfo in DataFileUtilities.ReadDataFile(indexFile))
                //Parallel.ForEach(DataFileUtilities.ReadDataFile(indexFile), fileInfo =>
                {
                    //// If not using parallel.foreach you can to move the using statement outside the foreach delegate
                    //using (var verifier = new AleDatabaseVerification(destinationDbInstance, destinationDb, username, password))
                    //{
                    if (!pcrNumbers.Contains(fileInfo.PcrNumber))
                    {
                        try
                        {
                            var verificationResult = verifier.VerifyPcrEntry(fileInfo);
                            verifier.AddResult(verificationResult);
                            pcrNumbers.Add(fileInfo.PcrNumber);
                        }
                        catch (Exception x)
                        {
                            log.Error("Error verifying PCR entry", x);
                        }
                        finally
                        {
                            if (pcrNumbers.Count % 100 == 0)
                            {
                                await verifier.AcceptChanges();
                                log.Info($"{pcrNumbers.Count} files processed at {pcrNumbers.Count / (DateTime.UtcNow - start).TotalSeconds} per second");
                                pcrNumbers.StreamToJson(progressFileName);
                            }
                        }
                    }
                }
                //});
            }
            Console.WriteLine($"Read {pcrNumbers.Count} records in : {(DateTime.UtcNow - start).TotalSeconds} seconds");
            pcrNumbers.StreamToJson(progressFileName);
        }
        
        internal static IEnumerable<string> ExtractSpecificPcrs(string indexFile)
        {
            foreach (var info in DataFileUtilities.ReadDataFile(indexFile))
            {
                yield return info.PcrNumber;
            }
        }

        internal static void SelectSpecificRecords(string indexFile, string requiredPatternAddresses, string outputFile)
        {
            DateTime start = DateTime.UtcNow;
            HashSet<ulong> pcrNumbers = File.Exists(progressFileName) ? JsonConvert.DeserializeObject<HashSet<ulong>>(File.ReadAllText(requiredPatternAddresses)) : new HashSet<ulong>();

            GetRequestedFiles(indexFile, pcrNumbers).StreamToJson(outputFile);

            Console.WriteLine($"Read {pcrNumbers.Count} records in : {(DateTime.UtcNow - start).TotalSeconds} seconds");
        }

        private static IDictionary<string, PcrPrintFileInformation> GetRequestedFiles(string indexFile, HashSet<ulong> patternAddresses)
        {
            ConcurrentDictionary<string, PcrPrintFileInformation> dic = new ConcurrentDictionary<string, PcrPrintFileInformation>();

            Parallel.ForEach(DataFileUtilities.ReadDataFile(indexFile), fileInfo =>
            {
                if (patternAddresses.Contains(fileInfo.PageAddresses.First(p => p.PageNumber == 1).PageAddressValue) && !dic.TryAdd(fileInfo.PcrNumber, fileInfo))
                {
                    throw new Exception("Duplicate Key Found");
                }
            });

            return dic;
        }
    }
}