﻿using System;

namespace PCRUtilities
{
    public class PcrPrintFileInformation
    {
        public string FilePath { get; set; }
        public string PcrNumber { get; set; }
        public PageAddress[] PageAddresses { get; set; }
        public int? FormBillId { get; set; }
        public int? FormSubmissionBillId { get; set; }
        public int? AnswerBillId { get; set; }
        public string AnswerPcr { get; set; }
        public string AnswerSubmissionPcr { get; set; }
        public bool IsCompletelyMapped
        {
            get
            {
                return (FormBillId.HasValue &&
                        FormSubmissionBillId.HasValue &&
                        AnswerBillId.HasValue &&
                        !string.IsNullOrWhiteSpace(AnswerSubmissionPcr) &&
                        !string.IsNullOrWhiteSpace(AnswerPcr));
            }
        }

        internal PcrPrintFileInfoValidationResponse Validate()
        {
            var response = new PcrPrintFileInfoValidationResponse();

            if (!AreEqual(FormBillId, FormSubmissionBillId) || !AreEqual(FormBillId, AnswerBillId))
            {
                response.ValidationErrors.Add($"Conflicting Bill Id's found: form='{FormBillId}' form_submission='{FormSubmissionBillId}' answer='{AnswerBillId}'");
            }

            if (!AreEqual(AnswerPcr, AnswerSubmissionPcr) || !AreEqual(AnswerPcr, PcrNumber))
            {
                response.ValidationErrors.Add($"Conflicting PCR numbers found: answer='{AnswerPcr}' answer_submission='{AnswerSubmissionPcr}' file='{PcrNumber}'");
            }

            return response;
        }

        private bool AreEqual(int? first, int? second)
        {
            bool result = first == second;
            if (!result)
            {
                if (!first.HasValue || !second.HasValue)
                {
                    // in this case, if either are missing that is OK
                    result = true;
                }
            }

            return result;
        }

        private bool AreEqual(string first, string second)
        {
            bool result = first == second;
            if (!result)
            {
                if (string.IsNullOrWhiteSpace(first) || string.IsNullOrWhiteSpace(second))
                {
                    // in this case, if either are missing that is OK
                    result = true;
                }
            }

            return result;
        }

        internal int? GetBillId()
        {
            int? retVal = null;
            if (Validate().IsValid)
            {
                retVal = FormBillId ?? FormSubmissionBillId ?? AnswerBillId;
            }

            return retVal;
        }
    }
}