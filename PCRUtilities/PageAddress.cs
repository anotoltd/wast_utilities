﻿namespace PCRUtilities
{
    public class PageAddress
    {
        public int PageNumber { get; set; }
        public string PatternPageAddress { get; set; }
        public ulong PageAddressValue { get; set; }

        internal void SetPageAddress(string patternAddress)
        {
            PatternPageAddress = patternAddress;
            PageAddressValue = GetAddressValueFromAddress(patternAddress);
        }

        public static string GetPageAddressFromValue(ulong value)
        {
            Anoto.Live.Ink.PageAddress pa = new Anoto.Live.Ink.PageAddress() { Value = value };
            return pa.ToString();
        }

        public static ulong GetAddressValueFromAddress(string address)
        {
            Anoto.Live.Ink.PageAddress pa = new Anoto.Live.Ink.PageAddress(address);
            return pa.Value;
        }
    }
}